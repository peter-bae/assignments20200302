package com.example.myapplication;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "My First App";
    boolean debug = true;
    TextView tvMyFirstApp;

    private static int count = 0;

    @Override
    protected void onStart() {
        super.onStart();
        count++;
        Log.i(TAG, "onStart is the " + count + " evoked method");

    }

    @Override
    protected void onResume() {
        super.onResume();
        count++;
        Log.i(TAG, "onResume is the " + count + " evoked method");

    }

    @Override
    protected void onPause() {
        super.onPause();
        count++;
        Log.i(TAG, "onPause is the " + count + " evoked method");

    }

    @Override
    protected void onStop() {
        super.onStop();
        count++;
        Log.i(TAG, "onStop is the " + count + " evoked method");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        count++;
        Log.i(TAG, "onDestroy is the " + count + " evoked method");

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        count++;
        Log.i(TAG, "onCreate is the " + count + " evoked method");
        Log.i(TAG, "Everything OK");


        debug = !debug;

       // tvMyFirstApp = findViewById(R.id.tvMyFirstApp);

        try{
            tvMyFirstApp.setText("This is going to crash");
        } catch (Exception error) {
            Log.d(TAG, "My error is " +error);
        }



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
